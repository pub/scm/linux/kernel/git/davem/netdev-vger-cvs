#ifndef __ASM_CRIS_SCATTERLIST_H
#define __ASM_CRIS_SCATTERLIST_H

struct scatterlist {
	struct page * page; /* Location of data */
	unsigned int offset;/* page offset */
	unsigned int length;
};

/* i386 junk */

#define ISA_DMA_THRESHOLD (0x1fffffff)

#endif /* !(__ASM_CRIS_SCATTERLIST_H) */
