/*
 * include/linux/simp_i.h  -- simple allocator for cached objects
 *
 * Internal things, not for external use.
 *
 * (C) 1997 Thomas Schoebel-Theuer
 */

#define SIMP_NUM_ORDER ((SIMP_MAX_ORDER)-(SIMP_MIN_ORDER)+1)
#define CHUNK_SIZE(ORDER) (PAGE_SIZE*(1<<(ORDER)))
#define CHUNK_BASE(MODE,ptr,size) (struct simp_header*)(((unsigned long)(ptr)) & ~((size)-1))
#define __CHUNK_BASE(ptr,simp) (struct simp_header*)(((unsigned long)(ptr)) & (simp)->hdr_mask)

#ifdef __SMP__
#define NR_PROCESSORS  NR_CPUS
#define _SMP(x) x
#define GLOBAL_ORDER 4
#define POSTBUFFER_SIZE 31
#else
#define NR_PROCESSORS  1
#define _SMP(x) /*nothing*/
#define GLOBAL_ORDER 1
#define POSTBUFFER_SIZE 127
#endif

#ifdef SIMP_DEBUG
#define _SIMP_DEBUG(x) x
#else
#define _SIMP_DEBUG(x) /*nothing*/
#endif

#ifdef DEAD_BEEF
#define _DEAD_BEEF(x) x
#else
#define _DEAD_BEEF(x) /*nothing*/
#endif

struct per_cpu {
	void ** buffer_pos;
	void * postbuffer[POSTBUFFER_SIZE];
};

struct simp_header {
	/* 1st cache line */
	struct simp * father;
	struct simp_header * next;
	void ** index;
	void ** fresh;
	void ** emptypos;
	structor first_ctor;
	void * fill[2];
#ifdef SIMP_DEBUG
	/* 2nd cache line */
	char magic[32];
#endif
};

struct simp {
	/* 1st cache line */
	struct simp_header * usable_list;
	spinlock_t lock;
	unsigned long hdr_mask;
	char fill[sizeof(void*) - sizeof(spinlock_t)];
	long real_size;
	long max_elems;
	long order;
	long chunksize;
	structor first_ctor;
	/* 2nd cache line */
	long create_offset;
	long color;
	long max_color;
	long size;
	long nr_header;
	long reserve;
	structor dtor;
	char * name;
	/* next cache lines */
	struct per_cpu private[NR_PROCESSORS];
};

